package datos;

public class Empresa {
	private static Empresa instanciaEmpresa;
	protected String nombre;
	protected String email;
	
	protected Empresa(){
		this.inicializar();
	}
	
	private void inicializar() { //Pueden leer la instancia de un archivo XML
		this.setNombre("Soft Argentina");
		this.setEmail("softargentina@unla.edu.ar");
	}
	
	public static Empresa getInstanciaEmpresa() {
		if (instanciaEmpresa == null) {
			instanciaEmpresa = new Empresa();
		}
		return instanciaEmpresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
